Snips quality assistant challenge
=================================

## How to

```bash
npm install
npm run dev     # webpack dev server with hot reload served on http://localhost:8080
npm run build   # production build
```