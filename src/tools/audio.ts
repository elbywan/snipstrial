const Recorder = require("self/libs/recorder.min.js")

const processChunks = data => {
    const audioData = new Blob(data, { "type" : "audio/ogg; codecs=opus" })
    return audioData
}

export const Audio = {
    available: () => !!navigator.mediaDevices.getUserMedia,
    record: () => {
        const dataChunks = []

        const recorderPromise = navigator.mediaDevices.getUserMedia({ audio: true }).then(stream => new window["MediaRecorder"](stream))

        const chunksPromise = recorderPromise.then(recorder => {
            if(!recorder) throw new Error("MediaRecorder is not supported")

            recorder.ondataavailable = e => dataChunks.push(e.data)
            recorder.start()
            return new Promise((res, rej) => {
                recorder.onstop = res
                recorder.onerror = e => rej(e.name)
            })
        }).then(() => processChunks(dataChunks))

        return [ recorderPromise, chunksPromise ]
    },
    isRecording: recorder => {
        return recorder.state === "recording"
    },
    isPaused: recorder => {
        return recorder.state === "paused"
    },
    isInactive: recorder => {
        return recorder.state === "inactive"
    },
    stop: recorder => {
        recorder.stop()
    }
}

// Failed attempt at adding https://github.com/chris-rudmin/Recorderjs
export const Audio2 = {
    available: Recorder.isRecordingSupported,
    record: () => {
        const dataChunks = []
        const recorder = new Recorder({
            encoderPath: "/libs/encoderWorker.min.js"
        })

        const recorderPromise = Promise.resolve(recorder)

        const chunksPromise = recorderPromise.then(recorder => {
            if(!recorder) throw new Error("MediaRecorder is not supported")

            recorder.addEventListener("dataAvailable", e => dataChunks.push(e.detail))
            recorder.addEventListener( "streamReady", () => {
                recorder.start()
            })
            recorder.state = "recording"

            return recorder.initStream().then(() => {
                new Promise((res, rej) => {
                    recorder.addEventListener("stop", () => {
                        recorder.state = "inactive"
                        res()
                    })
                    recorder.addEventListener("streamError", e => rej(e.name))
                })
            })
        }).then(() => processChunks(dataChunks))

        return [ recorderPromise, chunksPromise ]
    },
    isRecording: recorder => {
        return recorder.state === "recording"
    },
    isPaused: recorder => {
        return recorder.state === "paused"
    },
    isInactive: recorder => {
        return recorder.state === "inactive"
    },
    stop: recorder => {
        recorder.stop()
    }
}