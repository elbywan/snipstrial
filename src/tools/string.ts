export const string = {
    splitBySlots: (text: string, slots: Array<{range: number[]}>) => {
        const result = []
        let index = 0
        if(!slots || !slots.length) return [text]
        slots.forEach(slot => {
            if(index < slot.range[0])
                result.push({ text: text.substring(index, slot.range[0]) })
            result.push({ text: text.substring(slot.range[0], slot.range[1]), slot: true })
            index = slot.range[1]
        })
        if(index < text.length)
            result.push({ text: text.substring(index, text.length) })
        return result
    }
}