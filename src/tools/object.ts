export const object = {
    shallowCompare: (obj: object, obj2: object, excludes?: string[]) => {
        let equals = true
        for(const key in obj) {
            if(obj.hasOwnProperty(key)) {
                if(!(excludes && excludes.indexOf(key) >= 0) && obj[key] !== obj2[key]) {
                    equals = false
                    return
                }
            }
        }
        return equals
    },
    deepCompare: (obj: object, obj2: object, excludes?: string[]) => {
        for(const key in obj) {
            if(obj.hasOwnProperty(key)) {
                if(!(excludes && excludes.indexOf(key) >= 0)) {
                    if(typeof obj[key] === "object" && typeof obj2[key] === "object") {
                        if(obj[key] instanceof Array && obj2[key] instanceof Array) {
                            if(obj[key].length !== obj2[key].length) return false
                            for(let i = 0; i < obj[key]; i++) {
                                for(let j = 0; j < obj2[key]; j++) {
                                    if(!object.deepCompare(obj[key][i], obj2[key][j], excludes)) return false
                                }
                            }
                        } else {
                            if(!object.deepCompare(obj[key], obj2[key], excludes)) return false
                        }
                    } else {
                        if(obj[key] !== obj2[key]) return false
                    }
                }
            }
        }
        return true
    }
}