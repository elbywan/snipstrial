export const css = {
    buttonSpinner: (expectedStatus, status) => {
        if(status === expectedStatus) return " is-loading"
        return ""
    },
    buttonDisabler: (expectedStatus, status) => {
        return status && status !== expectedStatus
    }
}