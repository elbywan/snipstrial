export const blob = {
    blobToData: (blob: Blob) => {
        const reader = new FileReader()
        return new Promise(res => {
            reader.addEventListener("loadend", _ => res(reader.result))
            reader.readAsDataURL(blob)
        })
    },
    dataToBlob: (data: string) => {
        const byteString = atob(data.split(",")[1])
        const mime = data.split(",")[0].split(":")[1].split(";")[0]
        const ab = new ArrayBuffer(byteString.length)
        var ia = new Uint8Array(ab)

        for (let i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i)
        }

        return new Blob([ab], { type: mime });
    }
}