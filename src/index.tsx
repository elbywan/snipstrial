import * as React from "react"
import * as ReactDOM from "react-dom"
import { App } from "./components"
import { AppContainer } from "react-hot-loader"
import { Provider } from "react-redux"
import store from "self/store"

import "./style/style.scss"

const render = Component => element => {
    element && ReactDOM.render(
        <AppContainer>
            <Provider store={ store }>
                <Component/>
            </Provider>
        </AppContainer>,
        element
    )
}

render(App)(document.getElementById("app-root"))

// Hot Module Replacement API
if(module["hot"] && typeof module["hot"].accept === "function") {
    module["hot"].accept("./components/App/App", () => {
        render(App)(document.getElementById("app-root"))
    })
}