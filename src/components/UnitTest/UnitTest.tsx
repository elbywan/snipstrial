import * as React from "react"
import { Utterance, Recorder, Player } from "self/components"
import { css } from "self/tools"

import "./UnitTest.scss"

const renderHeader = test => {
    if(test.status){
        return <div className="has-text-primary">{ test.status }...</div>
    } else if(test.text && (test.asr === false || test.nlu === false)) {
        return <div className="has-text-danger">Failing</div>
    } else if(test.text && test.asr && test.nlu) {
        return <div className="has-text-success ">Successful</div>
    } else if(test.text && !test.asr) {
        return <div className="has-text-info ">Submitted</div>
    } else {
        return <div className="has-text-info">Recorded</div>
    }
}

const renderUtterance = test => {
    return test.text ? <Utterance text={ test.text } slots={ test.slots } /> : null
}

const renderRecorder = (onStartRecording, onRecorded, status) => {
    return <Recorder onRecorded={ onRecorded } onStartRecording={ onStartRecording } status={ status }/>
}

const renderAudio = test => {
    return test.track ? <Player source={ test.track } status={ test.status }/> : null
}

const renderCheckboxes = (test, onValidate) => {
    if(!test.text) return null
    return (
        <div className="validation-checks">
            <span className="validation-text">Validate the test results</span>
            <div className="field">
                <label>Automatic Speech Recognition</label>
                <button className={ "validate-button check-button" + (test.asr ? " selected" : "") }
                        onClick={ _ => onValidate("asr", true) }>✓</button>
                <button className={ "validate-button cross-button" + (test.asr === false ? " selected" : "") }
                        onClick={ _ => onValidate("asr", false) }>×</button>
            </div>
            <div className={"field" + (!test.asr ? " disabled" : "") }>
                <label>Natural Language Understanding</label>
                <button className={ "validate-button check-button" + (test.nlu ? " selected" : "") }
                        onClick={ _ => onValidate("nlu", true) } disabled={ !test.asr }>✓</button>
                <button className={ "validate-button cross-button" + (test.nlu === false ? " selected" : "") }
                        onClick={ _ => onValidate("nlu", false) } disabled={ !test.asr }>×</button>
            </div>
        </div>
    )
}

export type UnitTestProps = {
    data: any,
    onStartRecording: () => void,
    onRecorded: (Blob) => void,
    onDelete?: () => void,
    onTest?: () => void,
    onValidate?: (string, boolean) => void
}
export class UnitTest extends React.PureComponent<UnitTestProps> {

    render() {
        const data = this.props.data

        return data ? (
        <div className={ "UnitTest" + (data.status ? " processing" : "") }>
            <div className="test-header level">
                <div className="level-left"><div className="heading level-item  is-size-6">{ renderHeader(data) }</div></div>
                <div className="level-right">
                    <div className="buttons-bar level-item">
                        { renderAudio(data) }
                        { renderRecorder(this.props.onStartRecording, this.props.onRecorded, data.status) }
                        { data.track ?
                            <button className={ "button is-warning" + css.buttonSpinner("testing", data.status) }
                                    disabled={ css.buttonDisabler("testing", data.status) }
                                    onClick={ this.props.onTest }>
                                <i className="fa fa-cog" aria-hidden="true"></i>
                                <span>Test</span>
                            </button>
                            : null }
                        { this.props.onDelete ?
                            <button className="button tooltip is-tooltip-bottom is-tooltip-danger is-danger"
                                    onClick={ this.props.onDelete }
                                    disabled={ css.buttonDisabler("", data.status) }
                                    data-tooltip="Delete test">
                                <i className="fa fa-trash" aria-hidden="true"></i>
                                <span>Delete</span>
                            </button>
                            : null }
                    </div>
                </div>
            </div>
            <div className="test-body">
                <div className="test-contents">
                    { renderUtterance(data) }
                    { renderCheckboxes(data, this.props.onValidate) }
                </div>
            </div>
        </div>
        ) : <div className="UnitTest filler">
            { renderRecorder(this.props.onStartRecording, this.props.onRecorded, data && data.status) }
        </div>
    }

}