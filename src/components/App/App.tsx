import * as React from "react"
import { UnitTests } from "self/components"

export class App extends React.Component {

    render() {
        return <UnitTests />
    }

}