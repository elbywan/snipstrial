import * as React from "react"
import { css } from "self/tools"

export class Player extends React.PureComponent<{ source: Blob, status: string }> {

    state : {
        ready: boolean,
        playing: boolean,
        progress: number
    } = {
        ready: false,
        playing: false,
        progress: 0
    }

    audioTag: HTMLAudioElement = null

    componentDidMount() {
        this.setupAudio()
    }

    componentDidUpdate(previousProps) {
        if(previousProps.source !== this.props.source) {
            this.setupAudio()
        }
    }

    setupAudio() {
        this.audioTag.addEventListener("canplay", () => this.setState({ ready: true }))
        this.audioTag.addEventListener("ended", () => this.setState({ playing: false }))
        this.audioTag.src = window.URL.createObjectURL(this.props.source)
    }

    toggleAudio() {
        this.setState({ playing: !this.state.playing })
        if(this.state.playing)
            this.audioTag.pause()
        else
            this.audioTag.play()
    }

    render() {
        return (
            !this.props.source ? null :
            <div>
                <audio ref={ ref => ref ? this.audioTag = ref : null }></audio>
                {
                    this.audioTag && this.state.ready ?
                        !this.state.playing ?
                            <button className="record-button button tooltip is-tooltip-bottom is-tooltip-dark is-dark"
                                    onClick={ () => this.toggleAudio() }
                                    disabled={ css.buttonDisabler("", this.props.status) }
                                    data-tooltip="Play audio track">
                                <i className="fa fa-play-circle" aria-hidden="true"></i>
                                <span>Play</span>
                            </button> :
                        <button className="record-button button tooltip is-tooltip-bottom is-tooltip-dark is-dark"
                                onClick={ () => this.toggleAudio() }
                                disabled={ css.buttonDisabler("", this.props.status) }
                                data-tooltip="Pause audio track">
                            <i className="fa fa-pause" aria-hidden="true"></i>
                            <span>Stop</span>
                        </button> :
                    null
                }
            </div>
        )
    }
}