import * as React from "react"
import { Audio, css } from "self/tools"

import "./Recorder.scss"

export class Recorder extends React.PureComponent<{ onStartRecording: () => void, onRecorded: (Blob) => void, status: string }> {

    state = {
        error: null,
        recorder: null,
        track: null
    }

    async record(e) {
        try {
            const [ recorder, audio ] = await Audio.record()
            recorder.then(recorder => {
                this.props.onStartRecording()
                this.setState({ recorder: recorder })
            }).catch(err => this.setState({ error: err }))
            audio.then(data => {
                this.props.onRecorded(data)
            }).catch(err => this.setState({ error: err }))
        } catch(err) {
            this.setState({ error: err })
        }
    }

    stop(e) {
        Audio.stop(this.state.recorder)
        this.forceUpdate()
    }

    render() {
        if(!Audio.available()) {
            return <div>This browser does not support audio recording.</div>
        }
        if(this.state.error) {
            return <div className="has-text-danger">An error occured : { this.state.error.toString() }</div>
        }

        if(!this.state.recorder || Audio.isInactive(this.state.recorder))
            return <button  className={ "record-button button tooltip is-tooltip-bottom is-tooltip-info is-info" }
                            onClick={ e => this.record(e) }
                            disabled={ css.buttonDisabler("recording", this.props.status) }
                            data-tooltip="Record audio track">
                <i className="fa fa-microphone" aria-hidden="true"></i>
                <span>Record</span>
            </button>
        else if(Audio.isRecording(this.state.recorder)) {
            return <button  className="stop-button button tooltip is-tooltip-bottom is-tooltip-info is-info"
                            onClick={ e => this.stop(e) }
                            disabled={ css.buttonDisabler("recording", this.props.status) }
                            data-tooltip="Stop recording">
                <i className="fa fa-microphone-slash" aria-hidden="true"></i>
                <span>Stop</span>
            </button>
        }
    }

}