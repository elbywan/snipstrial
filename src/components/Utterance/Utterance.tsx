import * as React from "react"
import { string } from "self/tools"

import "./Utterance.scss"

export const Utterance = ({ text, slots }) => {
    return <div className="Utterance">
        { string.splitBySlots(text, slots).map((fragment, idx) =>
            fragment.slot ? <span className="slot" key={ idx }>{ fragment.text }</span> :
            fragment.text
        ) }
    </div>
}