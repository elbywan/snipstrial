import * as React from "react"
import { UnitTest } from "self/components"
import { connect, Dispatch } from "react-redux"
import * as actions from "self/store/actions"

import "./UnitTests.scss"

class _UnitTests extends React.PureComponent<{ data: any, dispatch: Dispatch }> {

    state = {
        testsRunning: false,
        runnerCount: null
    }

    componentDidMount() {
        this.props.dispatch(actions.populateTests())
    }

    startRecording(test) {
        if(!test) return
        this.props.dispatch(actions.startRecording(test))
    }

    recordTest(track, test) {
        this.props.dispatch(actions.addTrack(track, test))
    }

    performTest(test) {
        this.props.dispatch(actions.doTest(test))
    }

    deleteTest(test) {
        this.props.dispatch(actions.deleteTest(test))
    }

    validate(test, field, value) {
        this.props.dispatch(actions.validateTest(test, field, value))
    }

    async runAllTests() {
        this.setState({ testsRunning: true })
        for(let i = 0; i < this.props.data.tests.length; i++) {
            this.setState({ runnerCount: i })
            await this.props.dispatch(actions.doTest(this.props.data.tests[i]))
            if(!this.state.testsRunning) return
        }
        this.stopAllTests()
    }

    stopAllTests() {
        this.setState({ testsRunning: false })
    }

    getRunnerProgress() {
        return (this.state.runnerCount / this.props.data.tests.length) * 100
    }

    renderTestsStats() {
        const stats = this.props.data.tests.reduce((acc, test) => {
            return {
                total: acc.total + 1,
                submitted: acc.submitted + (test.text ? 1 : 0),
                failing: acc.failing + ((test.text && (test.asr === false || test.nlu === false)) ? 1 : 0),
                successful: acc.successful + ((test.text && test.asr && test.nlu) ? 1 : 0)
            }
        }, {
            total: 0,
            submitted: 0,
            failing: 0,
            successful: 0
        })

        return (
            <nav className="level">
                <div className="level-item has-text-centered">
                    <div>
                        <p className="heading">Tests</p>
                        <p className="title">{ stats.total }</p>
                    </div>
                </div>
                <div className="level-item has-text-centered has-text-info">
                    <div>
                        <p className="heading">Submitted</p>
                        <p className="title">{ stats.submitted }</p>
                    </div>
                </div>
                <div className="level-item has-text-centered has-text-danger">
                    <div>
                        <p className="heading">Failing</p>
                        <p className="title">{ stats.failing }</p>
                    </div>
                </div>
                <div className="level-item has-text-centered has-text-success">
                    <div>
                        <p className="heading">Successful</p>
                        <p className="title">{ stats.successful }</p>
                    </div>
                </div>
            </nav>
        )
    }

    renderAllTestsButtons() {
        return (
            <nav className="level">
                <div className="level-item has-text-centered runner-button-container">
                    {
                        !this.state.testsRunning ?
                            <button className="button runner-button" onClick={ e => this.runAllTests() }>
                                <span className="icon has-text-warning"><i className="fa fa-cogs" aria-hidden="true"></i></span>
                                <span>Run all tests</span>
                            </button> :
                            <button className="button runner-button" onClick={ e => this.stopAllTests() }>
                                <span className="icon has-text-warning"><i className="fa fa-cogs" aria-hidden="true"></i></span>
                                <span>Stop the test runner</span>
                            </button>
                    }
                </div>
            </nav>
        )
    }

    renderProgressText() {
        const count = this.state.runnerCount + 1
        const total = this.props.data.tests.length
        return !this.state.testsRunning ? null :
            <nav className="level has-text-centered">
                <div className="level-item field">
                    <div className="control">
                        <div className="tags has-addons">
                            <span className="tag">Processed</span>
                            <span className="tag is-warning">{ count }</span>
                        </div>
                    </div>
                    <div className="control">
                        <div className="tags has-addons">
                            <span className="tag">Total</span>
                            <span className="tag is-primary">{ total }</span>
                        </div>
                    </div>
                </div>
                {/* <span className="level-item">{ `${ count } / ${ total }` }</span> */}
            </nav>
    }

    renderProgressBar() {
        const runnerProgress = this.getRunnerProgress()

        return !this.state.testsRunning ? null :
            <nav className="level">
                <div className="level-item">
                    <progress className="progress is-small" value={ runnerProgress } max="100">{ runnerProgress }%</progress>
                </div>
            </nav>
    }

    render() {
        return (
        <div className="UnitTests">
            <section className="hero is-dark">
                <div className="hero-body">
                    <div className="container">
                        <h1 className="title">Unit Tests</h1>
                        <h2 className="subtitle">Create, manage and run your unit tests here.</h2>
                    </div>
                </div>
            </section>
            <section className="section">
                <div className="container">
                    {/* Stats */}
                    { this.renderTestsStats() }

                    {/* Test runner */}
                    { this.renderAllTestsButtons() }
                    { this.renderProgressText() }
                    { this.renderProgressBar() }

                    {/* Tests */}
                    { this.props.data.tests.map((test, idx) =>
                        <UnitTest
                            key={ idx }
                            data={ test }
                            onStartRecording={ () => this.startRecording(test) }
                            onRecorded={ data => this.recordTest(data, test) }
                            onDelete={ () => this.deleteTest(test) }
                            onTest={ () => this.performTest(test) }
                            onValidate={ (field, value) => this.validate(test, field, value) }/>
                    ) }

                    { /* Filler for adding a new test  */ }
                    <UnitTest data={ null }
                            onStartRecording={ () => this.startRecording(null) }
                            onRecorded={ data => this.recordTest(data, null) } />
                </div>
            </section>
        </div>
        )
    }

}

export const UnitTests = connect(({ unitTests }) => ({
    data: unitTests
}))(_UnitTests)