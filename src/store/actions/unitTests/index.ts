import { blob } from "self/tools"
import wretch from "wretch"
import uuid from "uuid/v4"
import { toJson, fromJson } from "self/storage/UnitTestsStorage"

export const IGNORE = ""
export const UNIT_TESTS_POPULATE        = "UNIT_TESTS_POPULATE"
export const UNIT_TEST_ADD_TRACK        = "UNIT_TEST_ADD_TRACK"
export const UNIT_TEST_START_RECORDING  = "UNIT_TEST_START_RECORDING"
export const UNIT_TEST_DELETE           = "UNIT_TEST_DELETE"
export const UNIT_TEST_START_TEST       = "UNIT_TEST_START_TEST"
export const UNIT_TEST_END_TEST         = "UNIT_TEST_END_TEST"
export const UNIT_TEST_VALIDATE         = "UNIT_TEST_VALIDATE"

export const populateTests = () => dispatch => {
    const sto = window.localStorage.getItem("UnitTests")
    if(!sto) {
        return wretch("/mock/unittests.json")
            .get()
            // Mocking an id which would be ideally returned by the server
            .json(payload => fromJson(JSON.stringify({ tests: payload, id: uuid() })))
            .then(data => {
                return toJson(data)
                    .then(data => window.localStorage.setItem("UnitTests", data))
                    .then(() => dispatch({
                        type: UNIT_TESTS_POPULATE,
                        data
                    }))
            })
    } else {
        return dispatch({
            type: UNIT_TESTS_POPULATE,
            data: fromJson(sto)
        })
    }
}

export const startRecording = test => ({
    type: UNIT_TEST_START_RECORDING,
    test
})

export const addTrack = (track, test) => ({
    type: UNIT_TEST_ADD_TRACK,
    track,
    test
})

export const deleteTest = test => ({
    type: UNIT_TEST_DELETE,
    test
})

export const doTest = test => dispatch => {
    if(test.status) {
        return { test: IGNORE }
    }

    dispatch({ type: UNIT_TEST_START_TEST, test })
    // Mocking response - normally would be a post request with the audio blob as the body
    return wretch("/mock/unittest.json")
        .get()
        // Mocking the id
        .json(payload => ({ ...payload, id: uuid() }))
        // Artificial delay
        .then(payload => new Promise(res => {
            setTimeout(() => res(payload), 500)
        }))
        .then(payload => dispatch({
            type: UNIT_TEST_END_TEST,
            test,
            payload
        }))
}

export const validateTest = (test, field, value) => ({
    type: UNIT_TEST_VALIDATE,
    test,
    field,
    value
})

