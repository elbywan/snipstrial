import reduceUnitTests from "./unitTests"

export default (state = { unitTests: undefined }, action) => {
    const reducedStore = {
        unitTests: reduceUnitTests(state.unitTests, action)
    }
    return reducedStore
}