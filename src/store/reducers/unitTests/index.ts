import * as actions from "self/store/actions"
import { object } from "self/tools"
import { setStorage } from "self/storage/UnitTestsStorage"
import uuid from "uuid/v4"

export default (unitTests = { tests: [] }, action) => {
    if(!action.type) {
        return unitTests
    }

    let reducedUnitTests = unitTests

    if(action.type === actions.UNIT_TESTS_POPULATE) {
        reducedUnitTests = action.data
    } else {
        reducedUnitTests = {
            tests: reduceTests(unitTests.tests, action)
        }
    }
    setStorage(reducedUnitTests)

    return reducedUnitTests

}

const doCacheResults = (test, payload) => test.text && object.deepCompare(test, payload, ["id"])

const reduceTests = (tests = [], action) => {
    switch(action.type) {
        case(actions.UNIT_TEST_START_RECORDING) :
            return tests.map(t => t !== action.test ? t : {
                ...t,
                status: "recording"
            })
        case(actions.UNIT_TEST_ADD_TRACK):
            return action.test ?
                tests.map(t =>
                    t.id !== action.test.id ? t : {
                        id: uuid(),
                        status: null,
                        track: action.track
                    }) :
                [ ...tests, { id: uuid(), track: action.track, status: null, } ]
        case(actions.UNIT_TEST_DELETE):
            return tests.filter(t => t.id !== action.test.id)
        case(actions.UNIT_TEST_START_TEST):
            return tests.map(t => t.id !== action.test.id ? t : {
                ...t,
                status: "testing"
            })
        case(actions.UNIT_TEST_END_TEST):
            return tests.map(t => t.id !== action.test.id || doCacheResults(action.payload, action.test) ?
                { ...t, status: null } : {
                ...action.payload,
                ...t,
                status: null,
                asr: null,
                nlu: null
            })
        case(actions.UNIT_TEST_VALIDATE):
            return tests.map(t => t.id !== action.test.id ? t : {
                ...t,
                [action.field]: action.value,
                nlu:
                    action.field === "asr" && !action.value ? false :
                    action.field === "nlu" ? action.value :
                    t.nlu
            })
        default:
            return tests
    }
}
