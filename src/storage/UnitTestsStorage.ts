import wretch from "wretch"
import { blob } from "self/tools"

export const toJson = async data => {
    const tests = await Promise.all(data.tests.map(async test => {
        if(test.track) {
            const blobTrack = await blob.blobToData(test.track)
            return { ...test, track: blobTrack, status: null }
        } else {
            return Promise.resolve(data.map(test => ({
                ...test,
                status: null
            })))
        }
    }))
    return Promise.resolve(JSON.stringify({
        ...data,
        tests: tests
    }))
}

export const fromJson = json => {
    const obj = JSON.parse(json)
    return {
        ...obj,
        tests: obj.tests.map(test => ({
            ...test,
            track: test.track ? blob.dataToBlob(test.track) : null
        }))
    }
}

export const getStorage = () => {
    const sto = window.localStorage.getItem("UnitTests")
    if(!sto) {
        return wretch("/mock/unittests.json")
            .get()
            .json(payload => fromJson(JSON.stringify({ tests: payload })))
            .then(data => {
                return toJson(data)
                    .then(data => window.localStorage.setItem("UnitTests", data))
                    .then(() => data)
        })
    } else {
        return Promise.resolve(fromJson(sto))
    }
}

export const setStorage = newStorage => {
    return toJson(newStorage)
        .then(data => window.localStorage.setItem("UnitTests", data))
        .then(() => newStorage)
}