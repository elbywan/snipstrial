const { resolve } = require("path")
const HtmlWebpackPlugin = require("html-webpack-plugin")
const ExtractTextPlugin = require("extract-text-webpack-plugin")
const CopyWebpackPlugin  = require("copy-webpack-plugin")

const src = (file = "") => resolve(__dirname, "..", "src", file)
const build = (file = "") => resolve(__dirname, "..", "build", file)

module.exports = {
    entry: {
        client: src("index.tsx"),
    },
    output: {
        path: build("static"),
        filename: "[name].min.js"
    },
    resolve: {
        extensions: [ ".js", ".jsx", ".ts", ".tsx" ],
        alias: {
            self: src()
        }
    },
    devtool: "source-map",
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                loader: "ts-loader",
                options: {
                    configFile: resolve(__dirname, "..", "tsconfig.json"),
                    compilerOptions: { declaration: false }
                }
            },
            {
                test: /\.scss$/,
                use: ExtractTextPlugin.extract([
                    "css-loader",
                    "sass-loader"
                ])
            },
        ]
    },
    plugins: [
        new ExtractTextPlugin("style.css"),
        new HtmlWebpackPlugin({
            template: src("index.html"),
            inject: true
        }),
        new CopyWebpackPlugin([
            {
                context: src("mock"),
                from: "**/*",
                to: build("mock")
            },
            {
                context: src("libs"),
                from: "**/*",
                to: build("libs")
            }
        ])
    ]
}
